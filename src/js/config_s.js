const fs = require("fs");
const utils = require("./utils.js");
const cells = require("./cell.js");

exports.createConfigPage = () => {

    const config = utils.loadJsonFile("config.json");

    let res ="<table id='config_table' style='text-align: center; width: 800px; padding-left: 10px;padding-top: 10px;'>\n";
    res += "<colgroup><col span='8' style='background-color:bisque'></colgroup>\n";
    res += "<tr>";
    res += "<th style='text-align: center;'>name</th>";
    res += "<th style='text-align: center;'>value</th>";
    res += "<th style='text-align: center;'>name</th>";
    res += "<th style='text-align: center;'>value</th>";
    res += "<th style='text-align: center;'>name</th>";
    res += "<th style='text-align: center;'>value</th>";
    res += "<th style='text-align: center;'>name</th>";
    res += "<th style='text-align: center;'>value</th>";
    res += "</tr>\n";

    res += `
        <tr>
            <td>eufy_server_ip:</td><td id="eufy_server_ip">${config.ws_server}</td>
            <td>eufy_server_port:</td><td id="eufy_server_port">${config.ws_port}</td>
            <td>control_port:</td><td id="control_port">${config.ws_ctrl_port}</td>
            <td>notification_port:</td><td id="notification_port">${config.ws_push_notification_port}</td>
        </tr>
        <tr>
            <td>get_video_hours:</td>
            <td>
                <input type='text' id='get_video_hours' value='${config.get_video_hours}' style='width:50px;'>
            </td>
            <td>download_timer(min):</td>
            <td>
                <input type='text' id='download_timer_minutes' value='${config.download_timer_minutes}' style='width:50px;'>
            </td>
            <td>heartbeat_timer(sec):</td>
            <td>
                <input type='text' id='heartbeat_timer_seconds' value='${config.heartbeat_timer_seconds}' style='width:50px;'>
            </td>
            <td>Schema_Version:</td>
            <td>
                <input type='text' id='schemaVersion' value='${config.schemaVersion}' style='width:40px;'>
            </td>
        </tr>
        <tr>
            <td>Download_Path:</td>
            <td>
                <input type='text' id='download_path' value='${config.download_path}' style='width:230px;'>
            </td>
            <td>keep_records(months):</td>
            <td>
                <input type='text' id='keep_record_months' value='${config.keep_record_months}' style='width:50px;'>
            </td>
            <td>default_device_sn:</td>
            <td>
                <input type='text' id='default_device_sn' value='${config.default_device_sn}' style='width:130px;'>
            </td>
            <td>log_obj_to_console:</td>
            <td>
                <input type='text' id='log_obj_to_console' value='${config.log_obj_to_console}' style='width:40px;'>
            </td>
        </tr>
        <tr>
            <td>log_file:</td>
            <td>
                <input type='text' id='log_file' value='${config.log_file}' style='width:230px;'>
            </td>
            <td>log_to_console:</td>
            <td>
                <input type='text' id='log_to_console' value='${config.log_to_console}' style='width:50px;'>
            </td>
            <td>restart_services:</td>
            <td>
                <input type='text' id='restart_services' value='${config.restart_services}' style='width:50px;'>
            </td>
            <td>event dpi:</td>
            <td>
                <input type='text' id='event_dpi' value='${config.event_dpi}' style='width:50px;'>
            </td>
        </tr>
    `;

    res += "</table>";

    res += `
        <div style='padding-left: 50px;padding-top: 10px;'>
            <table><tr>
                <td>
                    <button type="button" onclick='saveConfigChanges()'>Save</button>
                </td>
            </tr></table>
        </div>
    `;

    return res;
}

exports.createCameraPage = () => {

    let res = "";

    ////////  Cameras
    res += "<table id='camera_table' style='text-align: center; width: 300px; padding-left: 10px;padding-top: 10px;'>";
    res += "<colgroup><col span='8' style='background-color:bisque'></colgroup>";
    res += "<tr>";
    res += "<th style='text-align: center; width: 10%'>camera name</th>";
    res += "<th style='text-align: center; width: 20%'>device sn</th>";
    res += "<th style='text-align: center; width: 40%'>rtsp url</th>";
    res += "<th style='text-align: center; width: 40%'>rtsp dpi</th>";
    res += "<th style='text-align: center; width: 12%'>audio sample</th>";
    res += "<th style='text-align: center; width: 10%'>notification</th>";
    // res += "<th style='text-align: center; width: 15px'>cell class</th>";
    res += "<th style='text-align: center; width: 15px'>action</th>";
    res += "</tr>";

    for (let i = 0; i < utils.cameras.cameras.length; i++) {

        let checked = (utils.cameras.cameras[i].notify == true)?"checked":"";

        res += `
            <tr>
            <td>
                <input type='text' id='row${i}_name' value='${utils.cameras.cameras[i].name}' style='width:100px;'>
            </td>
            <td>
                <input type='text' id='row${i}_device_sn' value='${utils.cameras.cameras[i].device_sn}' style='width:150px;'>
            </td>
            <td>
                <input type='text' id='row${i}_url' value='${utils.cameras.cameras[i].url}' style='width:400px;'>
            </td>
            <td>
                <input type='text' id='row${i}_dpi' value='${utils.cameras.cameras[i].dpi}' style='width:80px;'>
            </td>
            <td>
                <input type='text' id='row${i}_audio_sample' value='${utils.cameras.cameras[i].audio_sample}' style='width:60px;'>
            </td>
            <td>
                <input type='checkbox' id='row${i}_notify' style='width:20px;' ${(utils.cameras.cameras[i].notify == true)?"checked":""}>
            </td>
            <!--
            <td>               
                <select id="device" name="device">
                    <option value="FrontDoor" selected>FrontDoor</option>
                    <option value="Driveway">Driveway</option>
                </select>
            </td>
            -->       
            <td>
                <table><tr>
                    <td><button type="button" onclick='moveUp(${i})'>Up</button></td>
                    <td><button type="button" onclick='moveDown(${i})'>Down</button></td>
                    <td><button type="button" onclick='deleteCamera(${i})'>Delete</button></td>
                </tr></table>
            </td>
            </tr>
        `;
    }

    res += "</table>";

    res += `
        <div style='padding-left: 50px;padding-top: 10px;'>
            <table><tr>
                <td>
                    <button type="button" onclick='addCamera()'>Add</button>
                    <button type="button" onclick='saveCameraChanges()'>Save</button>
                    <input type="hidden" id='numOfCamera' value='${utils.cameras.cameras.length}'/>
                </td>
                <td>
                </td>
            </tr></table>
        </div>
    `;


    ///// cells
    res += "<table id='cell_table' style='text-align: center; width: 300px; padding-left: 10px;padding-top: 10px;'>";
    res += "<colgroup><col span='14' style='background-color:bisque'></colgroup>";
    res += "<tr>";
    res += "<th style='text-align: center;'>name</th>";
    res += "<th style='text-align: center;'>type</th>";
    res += "<th style='text-align: center;'>width</th>";
    res += "<th style='text-align: center;'>height</th>";
    res += "<th style='text-align: center;'>y</th>";
    res += "<th style='text-align: center;'>x</th>";
    res += "<th style='text-align: center;'>url</th>";
    res += "<th style='text-align: center;'>icon</th>";
    res += "<th style='text-align: center;'>border left</th>";
    res += "<th style='text-align: center;'>border right</th>";
    res += "<th style='text-align: center;'>border top</th>";
    res += "<th style='text-align: center;'>border bottom</th>";
    res += "<th style='text-align: center;'>Enable</th>";
    res += "<th style='text-align: center;'>action</th>";
    res += "</tr>";

    for (let i = 0; i < cells.cells.cells.length; i++) {

        res += `
            <tr>
            <td>
                <input type='text' id='cell_row${i}_name' value='${cells.cells.cells[i].name}' style='width:100px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_type' value='${cells.cells.cells[i].type}' style='width:60px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_width' value='${!cells.cells.cells[i].width ? 1 : cells.cells.cells[i].width}' style='width:15px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_height' value='${!cells.cells.cells[i].height ? 1 : cells.cells.cells[i].height}' style='width:15px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_y' value='${!cells.cells.cells[i].y ? 0 : cells.cells.cells[i].y}' style='width:15px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_x' value='${!cells.cells.cells[i].x ? 0 : cells.cells.cells[i].x}' style='width:15px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_url' value="${!cells.cells.cells[i].url ? "" : cells.cells.cells[i].url}" style='width:200px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_icon' value='${!cells.cells.cells[i].icon ? "" : cells.cells.cells[i].icon}' style='width:100px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_left' value='${cells.cells.cells[i].border_left_width}' style='width:30px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_right' value='${cells.cells.cells[i].border_right_width}' style='width:30px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_top' value='${cells.cells.cells[i].border_top_width}' style='width:30px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_bottom' value='${cells.cells.cells[i].border_bottom_width}' style='width:30px;'>
            </td>
            <td>
                <input type='checkbox' id='cell_row${i}_enable' ${cells.cells.cells[i].enable ? "checked":""} style='width:30px;'>
            </td>
            <td>
                <table><tr>
                    <td><button type="button" onclick='moveCellUp(${i})'>Up</button></td>
                    <td><button type="button" onclick='moveCellDown(${i})'>Down</button></td>
                    <td><button type="button" onclick='deleteCell(${i})'>Delete</button></td>
                </tr></table>
            </td>
            </tr>
        `;
    }

    res += "</table>";

    res += `
        <div style='padding-left: 50px;padding-top: 10px;'>
            <table><tr>
                <td>
                    <button type="button" onclick='addCell()'>Add</button>
                    <button type="button" onclick='saveCellChanges()'>Save</button>
                    <input type="hidden" id='numOfCell' value='${cells.cells.cells.length}'/>
                </td>
                <td>
                </td>
            </tr></table>
        </div>
    `;

    res += "<br>";
    res += `
        <!--
        <div style='color: white'> Daemon status:</div>
        <div id='service_status' style='border: 2px solid blue; width=200px; height=200px; color:white;'>
        <br>
        </div>
        -->
    `;

    return res;
};



