// @ts-check
const { path: ffmpegPath } = require('@ffmpeg-installer/ffmpeg');
const { spawn } = require('child_process');
const ews = require('express-ws');
const ps = require('ps-node');
//const { version } = require('./package.json');
var fs = require('fs');

/**
 * @typedef {{
 *  url: string;
 *  additionalFlags?: string[];
 *  verbose?: boolean;
 *  transport?: 'udp' | 'tcp' | 'udp_multicast' | 'http';
 * windowsHide?: boolean;
 * }} Options
 *
 * @typedef {import("express").Application} Application
 * @typedef {import("ws")} WebSocket
 * @typedef {import("child_process").ChildProcessWithoutNullStreams} Stream
 */

class InboundStreamWrapper {
  constructor() {
    this.clients = 0;
    this.sockets = [];
  }

  start(options) {
    if (this.verbose) console.log('[rtsp-relay] Creating brand new stream');

    let additionalFlags = [];
    let windowsHide = true;

    // validate config
    const txpConfigInvalid = additionalFlags.indexOf('-rtsp_transport');
    // eslint-disable-next-line no-bitwise
    if (~txpConfigInvalid) {
      const val = additionalFlags[0o1 + txpConfigInvalid];
      console.warn(
        `[rtsp-relay] (!) Do not specify -rtsp_transport=${val} in additionalFlags, use the option \`transport: '${val}'\``,
      );
    }

    // // test ffmpeg
    // const targetFile = "C:\\Work\\Test\\tmp1\\out2.ts";
    // let TEST_FFMPEG = true;
    // if(TEST_FFMPEG)
    //   fs.open(targetFile, "w", function (err) {});

    //options.transport = 'tcp';
    if(options.camera.hasAudioFile) { // eufy video file + audio file
      this.stream = spawn(
        ffmpegPath,
        [
          ...(options.transport ? ['-rtsp_transport', options.transport] : []), // this must come before `-i [url]`, see #82
          '-i', options.url,        // video input
          '-i', options.url + 'a',  // audio input
          '-f', 'mpegts',           // output format
          '-codec:v', 'mpeg1video', // specify video codec (MPEG1 required for jsmpeg)
          // '-b:v', '200k',
          '-codec:a', 'mp2',        // specify audio codec
          '-ar', '44100',   // must be 44100 for eufy
          '-b:a', '128k',   // minimus 32k for eufy
          // '-ac',
          // '1',
          '-r', '30', // 30 fps. any lower and the client can't decode it
          '-vf', 'scale=' + options.camera.dpi + ':-1',
          ...additionalFlags,
          '-', // stream mode
          //'C:\\Work\\Test\\tmp1\\out1.ts',
          // '-f',
          // 'mp4',
          // '-codec:v',
          // 'mpeg4',
          // 'C:\\Work\\Test\\tmp1\\out1.mp4'
          
        ],
        { detached: false, windowsHide },
      );
    } else { // eufy rtsp
      this.stream = spawn(
        ffmpegPath,
        [
          ...(options.transport ? ['-rtsp_transport', options.transport] : []), // this must come before `-i [url]`, see #82
          '-i', options.url,
          '-f', 'mpegts', // force format
          '-codec:v', 'mpeg1video', // specify video codec (MPEG1 required for jsmpeg)
          '-codec:a', 'mp2', // specify audio codec
          ...(options.camera.audio_sample? ['-ar', options.camera.audio_sample] : []), //'-ar', '44100', // must be 44100 for eufy rtsp
          '-r', '30', // 30 fps. any lower and the client can't decode it
          '-vf', 'scale=' + options.camera.dpi + ':-1',
          ...additionalFlags,
          '-',
         ],
        { detached: false, windowsHide },
      );

    }

    this.stream.stderr.on('data', () => { });
    this.stream.stderr.on('error', (e) => console.log('[rtsp-relay] err:error', e));
    this.stream.stdout.on('error', (e) => console.log('[rtsp-relay] out:error', e));
    this.stream.stdout.on('data', (chunk) => {
      for (let i = 0; i < this.sockets.length; i++) {

        // readyState: 0 - CONNECTING, 1 - OPEN, 2 - CLOSING, 3 - CLOSED
        if (this.sockets[i].readyState === this.options.ws.OPEN) {
          this.sockets[i].send(chunk);
          // if(TEST_FFMPEG)
          //   fs.appendFileSync(targetFile, chunk, { encoding: "utf8", flag: "a+" });
        }
        else {
          if (this.verbose) console.log("[rtsp-relay] " + (i + 1).toString() + " of " + this.sockets.length.toString() + " : ws.readyState = " + this.sockets[i].readyState.toString());
          if(this.sockets[i].readyState === this.options.ws.CLOSED) {
            this.sockets.splice(i, 1);
            i--;
          }
        }
      }
    })
    this.stream.on('error', (err) => {
      if (this.verbose) {
        console.warn(`[rtsp-relay] Internal Error: ${err.message}`);
      }
    });

    this.stream.on('exit', (_code, signal) => {
      if (signal !== 'SIGTERM') {
        if (this.verbose) {
          console.warn(
            '[rtsp-relay] Stream died - will recreate when the next client connects. Clients: ' + this.clients.toString(),
          );

          setTimeout(()=> this.start(this.options), 1000);
        }

        this.stream = null;
      }
      else {
        if (this.verbose) {
          console.warn("[rtsp-relay] ffmpeg stream exits on SIGTERM!");
        }
      }
    });
  }

  // @param {Options} options
  get(options) {
    this.options = options;
    if (!this.sockets.includes(options.ws)) {
      this.sockets.push(options.ws);
    }
    this.verbose = options.verbose;
    this.clients += 1;
    if (!this.stream) this.start(options);
    return /** @type {Stream} */ (this.stream);
  }

  decrement() {
    this.clients -= 1;
    return this.clients;
  }

  forceKill() {
    if (this.stream) {
      this.stream.kill('SIGTERM');
      this.stream = null;
    }
  }

  // @param {number} clientsLeft
  kill(clientsLeft, ws) {
    if (!this.stream) return; // the stream is currently dead
    if (!clientsLeft) {
      if (this.verbose) {
        console.log('[rtsp-relay] no clients left; destroying stream');
      }
      this.stream.kill('SIGTERM');
      this.stream = null;
      // next time it is requested it will be recreated
      return;
    }

    if (this.verbose) {
      console.log(
        '[rtsp-relay] there are still some clients so not destroying stream',
      );
    }

    // remove ws from sockets
    const index = this.sockets.indexOf(ws);
    if(index > -1)
      this.sockets.splice(index, 1);
  }
}

// @type {ReturnType<ews>}
let wsInstance;

//
// @param {Application} app the express application
// @param {import("http").Server | import("https").Server} [server] optional - if you use HTTPS you will need to pass in the server
//
exports.rtsp_relay_func = (app, server) => {
  if (!wsInstance) wsInstance = ews(app, server);
  const wsServer = wsInstance.getWss();

  /**
   * This map stores all the streams in existance, keyed by the URL.
   * This means we only ever create one InboundStream per URL.
   * @type {{ [url: string]: InboundStreamWrapper }}
   */
  const Inbound = {};

  return {
    //
    //  * You must include a script tag in the HTML to import this script
    //  *
    //  * Alternatively, if you have set up a build process for front-end
    //  * code, you can import it instead:
    //  * ```js
    //  * import { loadPlayer } from "rtsp-relay/browser";
    //  * ```
    //
    //scriptUrl: `https://cdn.jsdelivr.net/npm/rtsp-relay@${version}/browser/index.js`,
    //scriptUrl: "/js?name=node_modules/rtsp-relay/browser/index.js",

    killAll() {
      ps.lookup({ command: 'ffmpeg' }, (err, list) => {
        if (err) throw err;
        list
          .filter((p) => p.arguments.includes('mpeg1video'))
          .forEach(({ pid }) => ps.kill(pid));
      });
    },

    forceKill(url){
      if(url && Inbound[url]) {
        Inbound[url].forceKill();
      }
    },

    proxy({ url, verbose, camera, ...options }) {
      if (!url) throw new Error('URL to rtsp stream is required');

      // TODO: node15 use ||=
      if (!Inbound[url]) Inbound[url] = new InboundStreamWrapper();

      // @param {WebSocket} ws
      function handler(ws) {
        // these should be detected from the source stream
        const [width, height] = [0x0, 0x0];

        const streamHeader = Buffer.alloc(0x8);
        streamHeader.write('jsmp');
        streamHeader.writeUInt16BE(width, 0x4);
        streamHeader.writeUInt16BE(height, 0x6);
        ws.send(streamHeader, { binary: true });

        if (verbose) console.log('[rtsp-relay] New WebSocket Connection');

        const streamIn = Inbound[url].get({ url, verbose, camera, ws, ...options });

        // // @param {Buffer} chunk
        // function onData(chunk) {
        //   if (ws.readyState === ws.OPEN)
        //     ws.send(chunk);
        //   else{
        //     if (verbose) console.log("[rtsp-relay] ws.readyState" + ws.readyState.toString());
        //   }
        // }

        ws.on('close', () => {
          const c = Inbound[url].decrement();
          if (verbose) {
            console.log(`[rtsp-relay] WebSocket Disconnected ${c} left`);
          }
          Inbound[url].kill(c, ws);
          //streamIn.stdout.off('data', onData);
        });

        //streamIn.stdout.on('data', onData);
      }
      return handler;
    },

  };
};
