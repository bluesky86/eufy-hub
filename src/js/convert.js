// @ts-check
const { path: ffmpegPath } = require('@ffmpeg-installer/ffmpeg');
const { spawn } = require('child_process');
const ps = require('ps-node');

class InboundStreamWrapper {
  constructor(options) {
    this.options = options;
  }

  start() {
    if (this.options.verbose) console.log('[ffmpeg-convert] Creating brand new stream');

    let windowsHide = true;

    if(this.options.hasAudioFile) {
      this.stream = spawn(
        ffmpegPath,
        [
          '-i', this.options.url,
          '-i', this.options.url + 'a',
          '-f', this.options.videoType,
          '-codec:v', 'h264',//'mpeg4', 'hevc',
          // '-r', '15', // 30 fps. any lower and the client can't decode it
          // '-vf', 'scale=' + this.options.dpi + ':-1',
          this.options.targetFile + '.' + this.options.videoType
        ],
        { detached: false, windowsHide },
      );
    } else {
      this.stream = spawn(
        ffmpegPath,
        [
          '-i', this.options.url,
          '-f', this.options.videoType,
          //'-codec:v', 'h264',//'mpeg4',
          // '-r', '30', // 30 fps. any lower and the client can't decode it
          // '-vf', 'scale=' + this.options.dpi + ':-1',
          this.options.targetFile + '.' + this.options.videoType        
        ],
        { detached: false, windowsHide },
      );

    }

    this.stream.stderr.on('data', () => { });
    this.stream.stderr.on('error', (e) => console.log('[ffmpeg-convert] err:error', e));
    this.stream.stdout.on('error', (e) => console.log('[ffmpeg-convert] out:error', e));
    this.stream.stdout.on('data', (chunk) => {
        console.log('[ffmpeg-convert] write date');
        // if(this.options.writeCallback) {
        //     this.options.writeCallback(chunk);
        // }
    });
    this.stream.on('error', (err) => {
      if (this.options.verbose) {
        console.log(`[ffmpeg-convert] Internal Error: ${err.message}`);
        if (this.options.errorCallback) {
            this.options.errorCallback(err.message);
            console.log('[ffmpeg-convert] exit');
        }
      }
    });

    this.stream.on('exit', (_code, signal) => {
      if (signal !== 'SIGTERM') {
        if (this.options.endCallback) {
            this.options.endCallback();
            console.log('[ffmpeg-convert] exit');
        }
    
        this.stream = null;
      }
      else {
        if (this.options.verbose) {
          console.warn("[ffmpeg-convert] ffmpeg stream exits on SIGTERM!");
        }
      }
    });
  }
}

exports.ffmpeg_convert_func = () => {

  const Inbound = {};

  return {

    killAll() {
      ps.lookup({ command: 'ffmpeg' }, (err, list) => {
        if (err) throw err;
        list
          .filter((p) => p.arguments.includes('mpeg1video'))
          .forEach(({ pid }) => ps.kill(pid));
      });
    },

    forceKill(url){
      if(url && Inbound[url]) {
        Inbound[url].forceKill();
      }
    },

    create({ ...options }) {
      if (!options.url) throw new Error('URL to convert is required');

      this.forceKill(options.url);
      
      Inbound[options.url] = new InboundStreamWrapper(options);

      return Inbound[options.url];
    },

  };
};
