const fs = require("fs");
const utils = require("../js/utils.js");


//const ip = utils.getIpAddress();

function createCell(cellObj, x, y) {
    // let class_str = "child" + cellObj.width.toString() + "x" + cellObj.height.toString();
    // let cell = `<div class='${class_str}' style='`;
    let cell = `<div class='child1x1' style='`;

    cell += "text-align:center;";
    cell += `border-left-width:${cellObj.border_left_width};`;
    cell += `border-top-width:${cellObj.border_top_width};`;
    cell += `border-right-width:${cellObj.border_right_width};`;
    cell += `border-bottom-width:${cellObj.border_bottom_width};'`;

    cell += ` x='${x}' y='${y}' w='${cellObj.width}' h='${cellObj.height}'`;

    cell += ">\n";

    if (cellObj.type == "cell")
        cell += cellObj.name;
    else if (cellObj.type == "mjpeg") {
        cell += ` 
        <canvas id='canvas_${cellObj.name}' class='video' style='display: block; width: 100%; height: 100%;' onclick='onVideoClick("canvas_${cellObj.name}")'></canvas>
        `;
    }
    else if (cellObj.type == "img") {
        cell += `
        <img src="${cellObj.name}" style="width:99.5%; height:99.5%;">
        `;
    }
    else if (cellObj.type == "nested") {
        for (let i = 0; i < cellObj.kids.length; i++) {
            cell += createCell(cellObj.kids[i], x, y);
        }
    }
    else if (cellObj.type == "iframe") {
        cell += `
        <div class="iframe" height="95%">
            ${cellObj.url}
        </div>
        `;

    }
    else if (cellObj.type == "link") {

        cell += `
            ${cellObj.name} <br> <a href="${cellObj.url}"><img src="${cellObj.icon}"><a>
        `;
    }
    else if (cellObj.type == "script") {
        cell += `
                ${cellObj.url}
        `;
    }
    else if (cellObj.type == "clock") {

        cell += `
            <div id='clock_div' style='color:rgb(224, 210, 210);font-size:30px;font-family:verdana;'></div>
            <script>
                function startTime() {
                    const today = new Date();
                    let year = today.getFullYear();
                    let month = new Intl.DateTimeFormat('en', { month: 'short' }).format(today);//today.getMonth() + 1;
                    let day = today.getDate();
                    let h = today.getHours();
                    let m = today.getMinutes();
                    let s = today.getSeconds();

                    if (m < 10) m = "0" + m;
                    if (s < 10) s = "0" + s;

                    let str = "<br>" + h + ":" + m + ":" + s + "<br><br>";
                    str += "<div style='font-size:20px;'>" + month + " " + day + ", " + year + "</div>";

                    document.getElementById('clock_div').innerHTML = str;

                    setTimeout(startTime, 1000);
                }

                startTime();

            </script>
        `;

    }
    else if (cellObj.type == "opener") {
        cell += `
            <div id="div_garage_door_opener">
                <br>${cellObj.name}<br>
                <div id="garage_door_opener_state">unknown</div>
                <button type="button" id="garage_door_opener_action" style="background-color: rgb(29, 51, 51);color:rgb(180, 180, 180);width:60px" onclick="garageDoorOpener()" >unknown</button>
            </div>
        `;
    }

    cell += "</div>\n";

    return cell;

}

function findCellByXY(cells, x, y) {

    for (let i = 0; i < cells.length; i++) {
        if (cells[i].enable && cells[i].x == x && cells[i].y == y)
            return i;
    }

    return -1;
}

exports.cells = utils.loadJsonFile("cell.json");

exports.createCells = () => {

    let gridBits = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];

    for (let i = 0; i < this.cells.cells.length; i++) {

        for (let h = 0; h < this.cells.cells[i].height; h++) {

            for (let w = 0; w < this.cells.cells[i].width; w++) {

                if (this.cells.cells[i].enable) {
                    if (gridBits[this.cells.cells[i].y + h][this.cells.cells[i].x + w] == 1)
                        console.log("Cell overlapped: x: " + (this.cells.cells[i].x + w).toString() + ", y: " + (this.cells.cells[i].y + h).toString());

                    gridBits[this.cells.cells[i].y + h][this.cells.cells[i].x + w] = 1;
                }

            }
        }
    }

    let contents = "";

    contents += `
        <script>
            function onVideoClick(id){

                let w = $("#main")[0].clientWidth;
                let h = $("#main")[0].clientHeight;
                let contents = "<canvas id='canvas_popup' class='video' style='display: block; width: " + w + "px;height: " + h + "px;' onclick='onCloseVideoClick();'></canvas>";

                $("#popup_video_section").html(contents);

                try {
                    let stream_port = $("#stream_port").val();
        
                    loadPlayer({
                        url: 'ws://' + window.location.hostname + ':' + stream_port + '/api/stream/' + id.substr(7, id.length - 7),
                        canvas: $("#canvas_popup")[0]
                    });
                }
                catch (err) {
                    console.log("loadPlayer error!");
                }
            }

            function onCloseVideoClick(){
                $("#popup_video_section").html("");
            }
        </script>
        <section id='popup_video_section'></section>
    `;

    contents += "<div style='position: relative; width: 100%; height: 100%;' >";

    for (let y = 0; y < 6; y++) {

        for (let x = 0; x < 9; x++) {

            let cell_id = findCellByXY(this.cells.cells, x, y);
            if (cell_id != -1)
                contents += createCell(this.cells.cells[cell_id], x, y);
            else if (gridBits[y][x] == 0) {
                contents += `
                    <div class='child1x1' style='border-left-width:1px; border-top-width:1px; border-right-width:1px;border-bottom-width:1px' x='${x}' y='${y}' w='1' h='1'></div>
                `;
            }
        }
    }

    contents += "</div>";

    return contents;

}

exports.findCellByName = (name) => {

    for (let i = 0; i < this.cells.cells.length; i++) {

        if (this.cells.cells[i].name == name)
            return this.cells.cells[i];
    }

    return null;
}
