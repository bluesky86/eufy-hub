const fs = require("fs");
const path = require("path");

const { exec } = require('child_process');

const utils = require("../js/utils.js");

const config = utils.loadJsonFile("config.json");

if (config.download_path[config.download_path.length - 1] != "/")
    config.download_path += "/";

exports.config = config;

exports.restartDaemons = (daemon) => {

    exec("sudo systemctl daemon-reload", (err, stdout, stderr) => {
        if (err) {
        } else {
        }
    });

    if (daemon == "server" ) {
        setTimeout(() => {
            exec("sudo systemctl restart eufy-security-ws.service", (err, stdout, stderr) => {
                if (err) {
                } else {
                }
            })
        }, 1000);
    }
    else if (daemon == "hub" ) {
        setTimeout(() => {
            exec("sudo systemctl restart eufy-hub.service", (err, stdout, stderr) => {
                if (err) {
                } else {
                }
            })
        }, 1000);
    }
    else if (daemon == "viewer" ) {

        setTimeout(() => {
            exec("sudo systemctl restart eufy-viewer.service", (err, stdout, stderr) => {
                if (err) {
                    //console.log(err.toString());
                } else {
                    //console.log(stdout.toString());
                }
            })
        }, 1000);
    }
    else if (daemon == "monitor" ) {

        setTimeout(() => {
            exec("sudo systemctl restart eufy-monitor.service", (err, stdout, stderr) => {
                if (err) {
                    //console.log(err.toString());
                } else {
                    //console.log(stdout.toString());
                }
            })
        }, 1000);
    }
    else if (daemon == "StopServer" ) {

        setTimeout(() => {
            exec("sudo systemctl stop eufy-security-ws.service", (err, stdout, stderr) => {
                if (err) {
                } else {
                }
            })
        }, 1000);
    }
    else if (daemon == "StopHub" ) {

        setTimeout(() => {
            exec("sudo systemctl stop eufy-hub.service", (err, stdout, stderr) => {
                if (err) {
                } else {
                }
            })
        }, 1000);
    }
    else if (daemon == "reboot" ) {

        setTimeout(() => {
            exec("sudo shutdown -r --force", (err, stdout, stderr) => {
                if (err) {
                } else {
                }
            })
        }, 1000);
    }


}

exports.getVideoUrl = (device, date, file) => {

    let path = config.download_path + utils.getDeviceSN(device) + "/" + date + "/" + file;
    if (fs.existsSync(path)) {
        return {
            result: "success",
            url: path,//"/stream?device=" + device + "&date=" + date + "&file=" + file,
            r_url: "/stream?device=" + device + "&date=" + date + "&file=" + file,
            hasAudioFile: fs.existsSync(path + 'a')
        };
    }
    else {
        return {
            result: "failure",
            url: ""
        };
    }

}

exports.getStream = (device, date, file) => {

    let path = config.download_path + utils.getDeviceSN(device) + "/" + date + "/" + file;
    if (fs.existsSync(path)) {

        let data = fs.readFileSync(path);

        return data;
    }

    return;

}

exports.getVideoList = (device_sn, forDate) => {

    const res = {};

    if (fs.existsSync(config.download_path)) {

        let deviceDirs = fs.readdirSync(config.download_path);
        for (let j = 0; j < deviceDirs.length; j++) {

            let devicePath = config.download_path + deviceDirs[j];

            if (!fs.lstatSync(devicePath).isDirectory())
                continue;
            if (deviceDirs[j] != device_sn)
                continue;

            devicePath += "/";

            let dirs = fs.readdirSync(devicePath);

            for (let i = 0; i < dirs.length; i++) {
                const datePath = devicePath + dirs[i];
                if (fs.lstatSync(datePath).isDirectory()) {

                    if (dirs[i].substr(0, 8).localeCompare(forDate) == 0) {

                        let files = fs.readdirSync(datePath);
                        for (let k = 0; k < files.length; k++) {
                            const ext = path.extname(files[k]);
                            if(ext === '.dat') {
                                const purl_name = path.basename(files[k], path.extname(files[k]));
                                const hasPerson = (purl_name.substr(15, 1) == "0") ? false : true;
                                const duration = purl_name.substr(17, purl_name.length - 17);
                                res[files[k]] = {
                                    filename: files[k],
                                    person: hasPerson,
                                    duration: duration,
                                    hasMp4: false,
                                    timestr: purl_name.substr(8, 2) + ":" + purl_name.substr(10, 2) + ":" + purl_name.substr(12, 2)
                                };
                            }
                            else if(ext === '.mp4') {
                                const purl_name = path.basename(files[k], path.extname(files[k]));
                                if(res[purl_name]) {
                                    res[purl_name].hasMp4 = true;
                                }
                            }
                        }

                    }
                }
            };
        }
    }
    else {
        console.log("The folder '" + config.download_path + "' doesnot exist!");
    }

    return { result: Object.values(res) };
}

