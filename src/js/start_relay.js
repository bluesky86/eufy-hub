const express = require('express');
const app = express();
// const fs = require("fs");
// const url = require('url');
// const ws1 = require("ws");

const utils = require("../js/utils.js");

let cameras = utils.loadJsonFile("camera.json");

const rtsp_relay = require("../js/rtsp_relay.js");

const proxy = rtsp_relay.rtsp_relay_func(app);

//const { proxy, scriptUrl } = require('rtsp_relay')(app);

function createHandlers() {

    for (let i = 0; i < cameras.cameras.length; i++) {
        cameras.cameras[i].handler = proxy.proxy({
            url: cameras.cameras[i].url,
            verbose: cameras.rtsp_relay_debug,
            camera: cameras.cameras[i],
        });

        // the endpoint our RTSP uses for this camera
        app.ws("/api/stream/" + cameras.cameras[i].name, cameras.cameras[i].handler);
    }
}

var playUrls = [];

exports.createHandle = (camera) => {

    camera.handler = proxy.proxy({
        url: camera.url,
        verbos: true,
        camera: camera
    });

    playUrls.push(camera.url);

    // the endpoint our RTSP uses for this camera
    app.ws("/api/stream/" + camera.name, camera.handler);

}

exports.forceKill = () => {
    playUrls.forEach((url) => {
        //console.log("kill " + url);
        proxy.forceKill(url);
    });
    playUrls = [];
}

createHandlers();

app.listen(cameras.port);

