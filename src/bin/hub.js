#!/usr/bin/env node
"use strict";

var fs = require('fs');
var ws = require('ws');
var promptly = require('promptly');
const { magenta } = require('ansi-colors');
const { clearInterval } = require('timers');
const path = require('path');
const { exec } = require('child_process');

const utils = require("../js/utils.js");

let connected = false;
let videoList = [];
let download_index = 0;
let buffer;

let dbFile = "db.txt";
let prevCachedVideoName = {};// = fs.readFileSync(dbFile).toString();
let diskTimer;
let heartBeatTimer = null;
let downloadTimer;
let downloadHeartBit_pre = 0;
let downloadHeartBit_cur = 0;
let reconnect_interval = 0;
let no_connect_count = 0;
let prev_push_timestamp = 0;

let config = utils.loadJsonFile("config.json");
if (config.download_path[config.download_path.length - 1] != "/")
    config.download_path += "/";

if (fs.existsSync(dbFile)) {
    let contents = fs.readFileSync(dbFile, "utf8").toString();
    if (contents.length > 0)
        prevCachedVideoName = JSON.parse(fs.readFileSync(dbFile, "utf8").toString());
    else
        prevCachedVideoName.data = [];
}

function getPrevVideoName(device_sn) {
    for (let i = 0; i < prevCachedVideoName.data.length; i++) {
        if (prevCachedVideoName.data[i].device_sn == device_sn)
            return prevCachedVideoName.data[i].file_name;
    }

    prevCachedVideoName.data.push(
        {
            device_sn: device_sn,
            file_name: ""
        }
    );

    return "";
}
function setPrevVideoName(device_sn, file_name) {
    for (let i = 0; i < prevCachedVideoName.data.length; i++) {
        if (prevCachedVideoName.data[i].device_sn == device_sn) {
            prevCachedVideoName.data[i].file_name = file_name;
            return;
        }
    }

    prevCachedVideoName.data.push(
        {
            device_sn: device_sn,
            file_name: file_name
        }
    );
}


function logmsg(msg) {
    msg = (new Date()).toLocaleString() + "  " + msg;
    if (config.log_to_console) {
        console.log(msg);
    }
    else {
        fs.writeFileSync(config.log_file, msg, { encoding: "utf8", flag: "w" });
    }
}

function restartServices(serverOnly) {

    exec("sudo systemctl daemon-reload", (err, stdout, stderr) => {
        if (err) {
            //some err occurred
            //console.error(err)
        } else {
            // the *entire* stdout and stderr (buffered)
            //console.log(`stdout: ${stdout}`);
            //console.log(`stderr: ${stderr}`);
        }
    });

    setTimeout(() => {
        exec("sudo systemctl restart eufy-security-ws.service", (err, stdout, stderr) => {
            if (err) {
            } else {
            }
        })
    }, 2000);

    if (serverOnly == false) {
        setTimeout(() => {
            exec("sudo systemctl restart eufy-viewer.service", (err, stdout, stderr) => {
                if (err) {
                } else {
                }
            })
        }, 1000);

        setTimeout(() => {
            exec("sudo systemctl restart eufy-hub.service", (err, stdout, stderr) => {
                if (err) {
                } else {
                }
            })
        }, 5000);
    }
}


/////////////////////////////////////////////////////////////////////////////
let ctrlUrl = "ws://" + config.ws_server + ":" + config.ws_ctrl_port;
let ctrl_ws = null;

let ctrl_connect = function (url) {

    ctrl_ws = new ws(ctrlUrl);

    ctrl_ws.onopen = () => {
        //ctrl_ws.send('shutdown');
    }

    ctrl_ws.onerror = (error) => {
        ctrl_ws.close();
        logmsg("Control WebSocket error");
    }

    ctrl_ws.onclose = (error) => {
        ctrl_ws = null;
        logmsg("Control WebSocket closed");
    }

    ctrl_ws.onmessage = (e) => {
        logmsg(e.data)
    }
}

/////////////////////////////////////////////////////////////////////////////


let wsUrl = "ws://" + config.ws_server + ":" + config.ws_port;

// let connectAttempts = 2147483647;
let restart_server_count = 0;
let continue_download_failure = 0;
let reconnect = 0;
let cur_device_sn = "";
var socket = null;
let connect = function (url) {

    connected = false;

    if (ctrl_ws == null)
        ctrl_connect(ctrlUrl);

    socket = new ws(url);

    start_heartbeat();

    socket.onerror = e => {

        connected = false;
        no_connect_count++;

        clearInterval(heartBeatTimer);
        heartBeatTimer = null;

        socket.close();
    };

    socket.onopen = function () {

        connected = true;
        downloadHeartBit_cur++;
        reconnect = 0;
        no_connect_count = 0;

        logmsg("WebSocket Client Connected");

        socket.send(JSON.stringify({
            messageId: "api-schema-id",
            command: "set_api_schema",
            schemaVersion: config.schemaVersion,
        }));
        socket.send(JSON.stringify({
            messageId: "start-listening-result",
            command: "start_listening",
        }));
    };

    socket.onclose = function () {

        logmsg("Socket.onclose");

        if (connected == false) {
            if (config.restart_services == 'true' && no_connect_count >= 12 * 5) {// 5 minutes
                restartServices(true); // restart server hub
                no_connect_count = 0;
                setTimeout(() => connect(url), 15 * 1000);
            }
            else {
                setTimeout(() => connect(url), 5 * 1000);
            }
        }
        else if (continue_download_failure < 3) {

            setTimeout(() => connect(url), reconnect_interval);

            continue_download_failure++;
        }
        else {

            continue_download_failure = 0;

            // restart service
            if (restart_server_count < 3) {
                if (config.restart_services == 'true') {
                    logmsg("Restart services (20s) ...");
                    restartServices(true);
                    if (ctrl_ws != null) ctrl_ws.close();
                    setTimeout(() => connect(url), 20 * 1000);
                }
                else {
                    logmsg("Send restart to server hub (7s) ...");
                    if (ctrl_ws != null) ctrl_ws.send("restart");
                    setTimeout(() => connect(url), 7 * 1000);
                }

                restart_server_count++;

            }
            else {

                // restart camera
                logmsg("Reboot camera (2m)..." + (cur_device_sn == "" ? config.default_device_sn : cur_device_sn));
                socket.send(JSON.stringify({
                    messageId: "reboot",
                    command: "station.reboot",
                    serialNumber: cur_device_sn == "" ? config.default_device_sn : cur_device_sn
                }));

                restart_server_count = 0;

                setTimeout(() => connect(url), 2 * 60 * 1000);
            }

        }

    };

    socket.onmessage = function (e) {

        const msg = JSON.parse(e.data.toString());// as OutgoingMessage;

        //logmsg(msg);
        //console.log(msg);

        if (msg.type == "event") {
            processEvents(msg);
        }
        else if (msg.type == "result") {
            processResults(msg);
        }
    };

    function processResults(msg) {

        if (msg.messageId == "start-listening-result") {
            // get video list, set interval for loop
            result_start_listening_result(msg);
        }
        else if (msg.messageId == "get_video_events") {
            // step 2
            result_get_video_events(msg);
        }
        // else if (msg.messageId == "start_download") {
        // }
        else if (msg.messageId == "disconnect") {

        }
        else if (msg.messageId == "connect") {

        }
        else {
            if (msg.messageId == "start_download" || msg.messageId == "api-schema-id") {

            }
            else {
                logmsg("unknown result: " + msg.messageId);
                if (config.log_obj_to_console == 'true') console.log(msg);
            }
        }

    }


    function result_start_listening_result(msg) {
        if (msg.success == true) {

            logmsg("Success: start_listening result");

            downloadHeartBit_cur++;

            getVideoList(config.get_video_hours * 3600);

        }
        else {
            logmsg("Failed: Start_listening result");

            clearInterval(heartBeatTimer);
            heartBeatTimer = null;

            // start timer to reconnect
            reconnect_interval = config.download_timer_minutes;

            socket.close();

        }
    }
    function result_get_video_events(msg) {
        if (msg.success == true) {

            downloadHeartBit_cur++;

            videoList = [];

            for (let i = 0; i < msg.result.events.length; i++) {
                let filePath = path.dirname(msg.result.events[i].storage_path) + "/";
                let fileName = path.basename(msg.result.events[i].storage_path);
                let event_time = fileName.substr(0, 4) + "-" + fileName.substr(4, 2) + "-" + fileName.substr(6, 2)
                    + " " + fileName.substr(8, 2) + ":" + fileName.substr(10, 2) + ":" + fileName.substr(12, 2);

                let duration = msg.result.events[i].end_time - msg.result.events[i].start_time;
                let targetFile = config.download_path + msg.result.events[i].device_sn + "/" + fileName.substr(0, 8) + "/"
                    + path.basename(fileName, path.extname(fileName)) + "_" + msg.result.events[i].has_human.toString();
                targetFile += "_" + duration.toString() + path.extname(fileName);

                videoList.push({
                    time: event_time,
                    path: filePath,
                    name: fileName,
                    subpath: fileName.substr(0, 8) + "/",
                    targetfile: targetFile,
                    cipher_id: msg.result.events[i].cipher_id,
                    device_sn: msg.result.events[i].device_sn,
                    person: msg.result.events[i].has_human == 0 ? false : true
                });

            }

            logmsg("videoNumber: " + videoList.length.toString());

            download_index = videoList.length - 1;
            //while (download_index >= 0 && videoList[download_index].name.localeCompare(prevCachedVideoName) <= 0)
            while (download_index >= 0 && videoList[download_index].name.localeCompare(getPrevVideoName(videoList[download_index].device_sn)) <= 0)
                download_index--;
            if (download_index >= 0) {
                socket.send(JSON.stringify({
                    messageId: "start_download",
                    command: "device.start_download",
                    serialNumber: videoList[download_index].device_sn,
                    path: videoList[download_index].path + videoList[download_index].name,
                    cipherId: videoList[download_index].cipher_id,
                }));

                cur_device_sn = videoList[download_index].device_sn;

            }
            else {
                logmsg("No more video to download.");

                clearInterval(heartBeatTimer);
                heartBeatTimer = null;

                setTimeout(() => getVideoList(config.get_video_hours * 3600), config.download_timer_minutes * 60 * 1000);
            }
        }
        else {
            logmsg("get video events failed!");
            if (config.log_obj_to_console == 'true') console.log(msg);

            clearInterval(heartBeatTimer);
            heartBeatTimer = null;

            reconnect_interval = 0;
            socket.close();
        }
    }

    function processEvents(msg) {
        if (msg.event.command == "start_download" && msg.event.returnCode == 0) {
            event_start_download(msg);
        }
        else if (msg.event.event == "download video data") {
            event_download_video_data(msg);
        }
        else if (msg.event.event == "download finished") {
            event_download_finished(msg);
        }
        else if(msg.event.event == "download audio data") {
            event_download_audio_data(msg);
        }
        else if (msg.event.event == "command result") {
            if (msg.event.command == "cancel_download") {
                logmsg("download canceled.");
            }
        }
        // else if (msg.event.event == "disconnected") {
        // }
        else if (msg.event.event == "property changed") {

            //console.log(msg);
            let notify_msg = "";
            if (msg.event.name == "motionDetected") {
                if (msg.event.value == true)
                    notify_msg = "motion detected";
            }
            else if (msg.event.name == "personDetected") {
                if (msg.event.value == true)
                    notify_msg = "person detected";
            }

            if (notify_msg != "") {
                if (utils.canNotification(msg.event.serialNumber)) {

                    for (let i = pushClients.length - 1; i >= 0; i--) {
                        if (pushClients[i].readyState == undefined || pushClients[i].readyState != 1)
                            continue;

                        if (prev_push_timestamp != msg.event.timestamp) {
                            prev_push_timestamp = msg.event.timestamp;
                            pushClients[i].send(notify_msg);
                            //console.log("HUB notify: " + msg.event.serialNumber);
                        }
                    }

                }
            }
        }
        else if (msg.event == "motion detected") {
            //console.log(msg.event);
        }
        else if (msg.event.event == "disconnected" || msg.event.event == "connected") {
            //console.log(msg);
        }
        else {
            if (config.log_obj_to_console == 'true'){
                logmsg("unknown event:");
                console.log(msg);
            } 
        }
    }

    function start_heartbeat() {

        clearInterval(heartBeatTimer);
        heartBeatTimer = null;

        downloadHeartBit_pre = downloadHeartBit_cur = 0;

        heartBeatTimer = setInterval(() => {

            if (downloadHeartBit_cur > downloadHeartBit_pre) {// download in progress
                downloadHeartBit_pre = downloadHeartBit_cur;
            }
            else {  // download paused???

                downloadHeartBit_pre = downloadHeartBit_cur = 0;

                clearInterval(heartBeatTimer);
                heartBeatTimer = null;

                logmsg("heart beat fail: Try to close socket ...");

                // 1 second
                reconnect_interval = 0;

                socket.close();

            }

        }, config.heartbeat_timer_seconds * 1000);
    }
    function event_start_download(msg) {

        downloadHeartBit_cur++;

        let ttt = path.dirname(videoList[download_index].targetfile);
        if (!fs.existsSync(ttt))
            fs.mkdirSync(path.dirname(videoList[download_index].targetfile), { recursive: true });

        // video file
        fs.open(videoList[download_index].targetfile, "w", function (err) {
            if (err) {
                logmsg("no such file or directory: " + videoList[download_index].device_sn + "/" + videoList[download_index].subpath + videoList[download_index].name);
            }
            else
                logmsg("create file: " + videoList[download_index].device_sn + "/" + videoList[download_index].subpath + videoList[download_index].name);

        });
        // audio file
        fs.open(videoList[download_index].targetfile + "a", "w", function (err) {
            if (err) {
                logmsg("no such file or directory: " + videoList[download_index].device_sn + "/" + videoList[download_index].subpath + videoList[download_index].name + "a");
            }
            else
                logmsg("create file: " + videoList[download_index].device_sn + "/" + videoList[download_index].subpath + videoList[download_index].name + "a");

        });
    }
    function event_download_video_data(msg) {

        downloadHeartBit_cur++;
        continue_download_failure = 0;

        buffer = new Uint8Array(msg.event.buffer.data.length);
        for (let i = 0; i < msg.event.buffer.data.length; i++) {
            buffer[i] = msg.event.buffer.data[i];
        }

        fs.appendFileSync(videoList[download_index].targetfile, buffer, { encoding: "utf8", flag: "a+" }, function (err) {
            if (err) {
                if (config.log_obj_to_console == 'true'){
                    logmsg("Append file error: ");
                     console.log(err);
                }
            }
            else {
                //logmsg("The file was saved!");
            }
        });

    }
    function event_download_audio_data(msg) {

        downloadHeartBit_cur++;
        continue_download_failure = 0;

        buffer = new Uint8Array(msg.event.buffer.data.length);
        for (let i = 0; i < msg.event.buffer.data.length; i++) {
            buffer[i] = msg.event.buffer.data[i];
        }

        fs.appendFileSync(videoList[download_index].targetfile + "a", buffer, { encoding: "utf8", flag: "a+" }, function (err) {
            if (err) {
                if (config.log_obj_to_console == 'true'){
                    logmsg("Append file error: ");
                     console.log(err);
                }
            }
            else {
                //logmsg("The file was saved!");
            }
        });

    }
    function event_download_finished(msg) {

        //clearInterval(heartBeatTimer);

        downloadHeartBit_cur++;
        continue_download_failure = 0;

        if (videoList[download_index].name.localeCompare(getPrevVideoName(videoList[download_index].device_sn)) > 0) {
            setPrevVideoName(videoList[download_index].device_sn, videoList[download_index].name);
            fs.writeFileSync(dbFile, JSON.stringify(prevCachedVideoName), { encoding: "utf8", flag: "w" });
        }

        logmsg("    Done: " + videoList[download_index].device_sn + "/" + videoList[download_index].subpath + videoList[download_index].name);

        // download next video
        download_index--;
        if (download_index >= 0) {
            socket.send(JSON.stringify({
                messageId: "start_download",
                command: "device.start_download",
                serialNumber: videoList[download_index].device_sn,
                path: videoList[download_index].path + videoList[download_index].name,
                cipherId: videoList[download_index].cipher_id,
            }));

            cur_device_sn = videoList[download_index].device_sn;
        }
        else {
            logmsg("Done for this list!");

            clearInterval(heartBeatTimer);
            heartBeatTimer = null;

            logmsg("Timer to get video list after " + config.download_timer_minutes + " minutes...")
            setTimeout(() => getVideoList(config.get_video_hours * 3600), config.download_timer_minutes * 60 * 1000);
        }
    }


    function getVideoList(second) {

        let startTime = new Date(new Date().getTime() - second * 1000);
        let endTime = new Date();

        if (heartBeatTimer == null)
            start_heartbeat();

        socket.send(JSON.stringify({
            messageId: "get_video_events",
            command: "driver.get_video_events",
            startTimestampMs: startTime,
            endTimestampMs: endTime
            //,filter: {deviceSN:"T8441P10211408D1", storageType:1}
            //,filter: {deviceSN:"T8441P10211308AA", storageType:1}
        }));

        /*
        export interface IncomingCommandGetVideoEvents extends IncomingCommandBase {
            command: DriverCommand.getVideoEvents;
            startTimestampMs?: number;
            endTimestampMs?: number;
            filter?: EventFilterType;
            maxResults?: number;
        }
        export interface EventFilterType {
            deviceSN?: string;
            stationSN?: string;
            storageType?: StorageType;
        }
        */

    }
}

connect(wsUrl);

// free disk
freeDisk();
// set timer to free disk for next day
diskTimer = setInterval(freeDisk, 24 * 3600);

function freeDisk() {

    let date1 = new Date();
    let year = date1.getFullYear();
    let month = date1.getMonth() + 1;
    let day = date1.getDate();

    let baseYear = year;
    let baseMonth = month - config.keep_record_months;

    if (baseMonth <= 0) {
        baseYear--;
        baseMonth += 12;
    }

    let baseYearStr = baseYear.toString();
    let baseMonthStr = (baseMonth > 9) ? baseMonth.toString() : "0" + baseMonth.toString();
    let baseDayStr = (day > 9) ? day.toString() : "0" + day.toString();

    let cmpDateStr = baseYearStr + baseMonthStr + baseDayStr;

    if (fs.existsSync(config.download_path)) {

        let deviceDirs = fs.readdirSync(config.download_path);
        for (let j = 0; j < deviceDirs.length; j++) {
            const devicePath = config.download_path + deviceDirs[j];
            if (!fs.lstatSync(devicePath).isDirectory())
                continue;
            let dirs = fs.readdirSync(devicePath);
            for (let i = 0; i < dirs.length; i++) {
                const datePath = devicePath + "/" + dirs[i];
                if (fs.lstatSync(datePath).isDirectory()) {

                    if (dirs[i].localeCompare(cmpDateStr) < 0) {

                        fs.rm(datePath, { recursive: true, force: true }, (err) => {
                            if (err)
                                logmsg(`Failed to delete ${datePath} !`);
                            else
                                logmsg(`${datePath} is deleted!`);
                        });
                    }
                }
            };
        }
    }
    else {
        logmsg("The folder '" + config.download_path + "' doesnot exist!");
    }

}


let closing = false;
const handleShutdown = () => {

    wsPush.close();

    // Pressing ctrl+c twice.
    if (closing) {
        process.exit();
    }

    // Close gracefully
    closing = true;

    clearInterval(heartBeatTimer);
    clearInterval(downloadTimer);
    clearInterval(diskTimer);

    socket.close();
    process.exit();
};
process.on("SIGINT", handleShutdown);
process.on("SIGTERM", handleShutdown);

//////////////////////////////////
//////        CLI         //////// 
//////////////////////////////////

(async () => {
    let cmd
    do {
        cmd = await promptly.prompt("eufy-security>");
        const args = cmd.split(" ");
        if (connected) {
            switch (args[0]) {
                case "help":
                    break;
                case "device.get_voices":
                    socket.send(JSON.stringify({
                        messageId: "get_voices",
                        command: "device.get_voices",
                        serialNumber: args[1],
                    }));
                break;
                case "device.get_history_events":
                    socket.send(JSON.stringify({
                        messageId: "get_history_events",
                        command: "device.get_history_events",
                        maxResults: 10
                    }));
                break;
                case "device.start_download":
                    if (args.length === 4) {

                        let pos = args[2].lastIndexOf("/");
                        videoList = [];
                        videoList.push({
                            time: "",
                            path: args[2].substr(0, pos + 1),
                            name: args[2].substr(pos + 1, args[2].length - pos - 1),
                            cipher_id: Number.parseInt(args[3]),
                            device_sn: args[1],
                            person: false
                        });

                        download_index = 0;

                        socket.send(JSON.stringify({
                            messageId: "start_download",
                            command: "device.start_download",
                            serialNumber: args[1],
                            path: args[2],
                            cipherId: Number.parseInt(args[3]),
                        }));

                        //start_heartbeat();
                    }
                    else {
                        logmsg("Format: 'command device_sn file_path cipher_id'");
                    }
                    break;
                case "device.cancel_download":
                    if (args.length === 2) {
                        socket.send(JSON.stringify({
                            messageId: "cancel_download",
                            command: "device.cancel_download",
                            serialNumber: args[1],
                        }));
                    }
                    else
                        logmsg("Format: 'command device_sn'");
                    break;
                case "station.disconnect":
                    if (args.length === 2) {
                        socket.send(JSON.stringify({
                            messageId: "disconnect",
                            command: "station.disconnect",
                            serialNumber: args[1],
                        }));
                    }
                    else
                        logmsg("Format: 'command device_sn'");
                    break;
                case "station.connect":
                    if (args.length === 2) {
                        socket.send(JSON.stringify({
                            messageId: "connect",
                            command: "station.connect",
                            serialNumber: args[1],
                        }));
                    }
                    else
                        logmsg("Format: 'command device_sn'");
                    break;
                case "beep":
                    for (let i = 0; i < pushClients.length; i++) {
                        if (pushClients[i].state == 1);
                        pushClients[i].send("person detected");
                    }
                    break;
            }
        }
    } while (cmd !== "quit" && cmd !== "exit")

    handleShutdown();

})();


/////////////////////////////////////
////// push notification server /////
/////////////////////////////////////

const wsPush = new ws.Server({ port: config.ws_push_notification_port })

let pushClients = [];

wsPush.on("connection", wsc => {

    // remove disconnected clients
    let j = 0;
    for (let i = 0; i < pushClients.length; i++) {
        if (pushClients[i].readyState == 1) {
            if (i > j) {
                pushClients[j] = pushClients[i];
                j++;
            }
        }
    }
    // push new client
    pushClients.push(wsc);

    wsc.on("message", message => {
        console.log(message.toString());
        if (message == "restart") {
        }
        else if (message == "shutdown") {
        }
    })

    //wsc.send('Push notification server is runing..');
});
