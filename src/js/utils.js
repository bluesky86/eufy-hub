
const { networkInterfaces } = require('os');
const fs = require("fs");
const { url } = require('inspector');

// const disk = require('diskusage');

// // get disk usage. Takes mount point as first parameter
// disk.check('/workspaces', function(err, info) {
//     console.log(info.free);
//     console.log(info.total);
// });

exports.getIpAddress = () => {

    const nets = networkInterfaces();
    const results = Object.create(null); // Or just '{}', an empty object

    let ip = "";
    for (const name of Object.keys(nets)) {
        for (const net of nets[name]) {
            // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
            if (net.family === 'IPv4' && !net.internal && (name == "eth0" || name == "Wi-Fi")) {
                ip = net.address;
                break;
            }
        }
        if (ip != "")
            break;
    }
    if (ip == "") ip = "localhost";

    return ip;
}

exports.loadJsonFile = (file) => {

    if (!fs.existsSync(file))
        throw file + " is not available!";

    const contents = fs.readFileSync(file, "utf8").toString();
    let res = JSON.parse(contents);
    return res;
}

exports.cameras = this.loadJsonFile("camera.json");

exports.getDeviceSN = (device_name) => {

    for (let i = 0; i < this.cameras.cameras.length; i++) {

        if (this.cameras.cameras[i].name == device_name)
            return this.cameras.cameras[i].device_sn;
    }

    return "";
}

exports.getDeviceName = (device_sn) => {

    for (let i = 0; i < this.cameras.cameras.length; i++) {

        if (this.cameras.cameras[i].device_sn == device_sn)
            return this.cameras.cameras[i].name;
    }

    return "";
}

exports.canNotification = (device_sn) => {

    for (let i = 0; i < this.cameras.cameras.length; i++) {

        if (this.cameras.cameras[i].device_sn == device_sn){
            //console.log("Notification: " + device_sn + " = " + this.cameras.cameras[i].notify);
            return this.cameras.cameras[i].notify;
        }
    }

    return false;
 
}

exports.escapeAnd = (url) => {

    let res = "";

    for(let i = 0; i<url.length; i++){

        if(url[i] == '&'){
            res += "%26";
        }
        else
            res += url[i];
    }

    return res;

}

exports.decodeURLString = (url) => {

    let res = "";

    for(let i = 0; i<url.length; i++){

        if(url[i] == '%'){

            if(url[i+1] == '2'){
                if(url[i+2] == "0"){
                    res += ' ';
                    i += 2;
                }
                else if(url[i+2] == '1'){
                    res += '!';
                    i += 2;
                }
                else if(url[i+2] == '2'){
                    res += '"';
                    i += 2;
                }
                else if(url[i+2] == '3'){
                    res += '#';
                    i += 2;
                }
                else if(url[i+2] == '4'){
                    res += '$';
                    i += 2;
                }
                else if(url[i+2] == '5'){
                    res += '%';
                    i += 2;
                }
                else if(url[i+2] == '6'){
                    res += '&';
                    i += 2;
                }
                else if(url[i+2] == '7'){
                    res += "'";
                    i += 2;
                }
                else if(url[i+2] == '8'){
                    res += '(';
                    i += 2;
                }
                else if(url[i+2] == '9'){
                    res += ')';
                    i += 2;
                }
                else {
                    res += url[i];
                }
            }
            else if(url[i+1] == '3'){
                if(url[i+2] == 'A'){
                    res += ':';
                    i += 2;
                }
                else if(url[i+2] == 'B'){
                    res += ';';
                    i += 2;
                }
                else if(url[i+2] == 'C'){
                    res += '<';
                    i += 2;
                }
                else if(url[i+2] == 'D'){
                    res += '=';
                    i += 2;
                }
                else if(url[i+2] == 'E'){
                    res += '>';
                    i += 2;
                }
                else if(url[i+2] == 'F'){
                    res += '?';
                    i += 2;
                }
                else{
                    res += url[i];
                }
            }
            else if(url[i+1] == '4'){
                if(url[i+1] == '0'){
                    res += '@';
                    i += 2;
                }
                else
                    res += url[i];
            }
            else if(url[i+1] == '5'){
                if(url[i+2] == 'B'){
                    res += '[';
                    i += 2;
                }
                else if(url[i+2] == 'C'){
                    res += '\\';
                    i += 2;
                }
                else if(url[i+2] == 'D'){
                    res += ']';
                    i += 2;
                }
                else if(url[i+2] == 'E'){
                    res += '^';
                    i += 2;
                }
                else{
                    res += url[i];
                }
            }
            else if(url[i+1] == '6'){
                if(url[i+2] == '0'){
                    res += '`';
                    i += 2;
                }
                else{
                    res += url[i];
                }
            }
            else if(url[i+1] == '7'){
                if(url[i+2] == 'B'){
                    res += '{';
                    i += 2;
                }
                else if(url[i+2] == 'D'){
                    res += '}';
                    i += 2;
                }
                else if(url[i+2] == 'E'){
                    res += '~';
                    i += 2;
                }
                else{
                    res += url[i];
                }
            }
            else
                res += url[i];
        }
        else if (url[i] == '&') {
            res += "%26";
        }
        else {
            res += url[i];
        }

    }

    return res;
}
