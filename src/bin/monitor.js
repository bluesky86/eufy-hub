#!/usr/bin/env node
"use strict";

const { clearInterval } = require('timers');
const path = require('path');
const { exec } = require('child_process');
const request = require('request');

// monitor hub.js
let selfMonitorTimer = setInterval( () => {
    //console.log(new Date().toLocaleString() + ": checking eufy-hub.service");
    exec(" systemctl status eufy-hub.service", (err, stdout, stderr) => {
        if (stdout) {
            //console.log(stdout.toString());
            if(stdout.toString().indexOf('Failed to start Eufy-Hub.service') != -1){
                console.log(new Date().toLocaleString() + ":  Eufy-Hub.service is not started - Failed to start Eufy-Hub.service");
                console.log(new Date().toLocaleString() + ":  Starting Eufy-Hub.service ...");
                exec( "sudo systemctl start eufy-hub.service", (err, stdout, stderr) =>{
                    console.log('sudo systemctl start eufy-hub.service');
                    if(stdout){
                        console.log(new Date().toLocaleString() + ":  eufy-hub.service is running");
                    }
                    if(err || stderr){
                        console.log(new Date().toLocaleString() + ":  Error to start eufy-hub.service");
                    }
                });
            }
            else{
                //console.log(new Date().toLocaleString() + ":  eufy-hub.service is running");
            }
        }

        if(stderr){
            console.log(new Date().toLocaleString() + ": Std Error:")
            console.log(stderr.toString());
        }
    })

}, 15 * 60 * 1000);

function updateIPAddress(){
    request('https://home86.000webhostapp.com/update.php', { json: false }, (err, res, body) => {
    if (err) { return console.log(err); }
        console.log('External IP Address: ' + body);
    });
}

let updateIPAddressTimer = setInterval( () => {
    updateIPAddress();
}, 86400 * 1000);

updateIPAddress();

let closing = false;
const handleShutdown = () => {

    // Pressing ctrl+c twice.
    if (closing) {
        process.exit();
    }

    // Close gracefully
    closing = true;

    clearInterval(selfMonitorTimer);
    clearInterval(updateIPAddressTimer);

     process.exit();
};
process.on("SIGINT", handleShutdown);
process.on("SIGTERM", handleShutdown);