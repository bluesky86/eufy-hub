
function adjustGrids() {

    let ew = (($("#main")[0].clientWidth - 3) / 9);
    let eh = (($("#main")[0].clientHeight - 3) / 6);

    let cells = $('.child1x1');
    for (let i = 0; i < cells.length; i++) {
        let x = parseInt(cells[i].getAttribute('x')) * ew;
        let y = parseInt(cells[i].getAttribute('y')) * eh;
        let w = parseInt(cells[i].getAttribute('w')) * ew;
        let h = parseInt(cells[i].getAttribute('h')) * eh;
        cells[i].style.position = 'absolute';
        cells[i].style.left = x + 'px';
        cells[i].style.top = y + 'px';
        cells[i].style.width = w + 'px';
        cells[i].style.height = h + 'px';
        cells[i].style.color = 'rgb(224, 210, 210)';
        cells[i].style.border = '1px solid black';
    }
}

$(document).ready(() => {

    window.addEventListener('resize', adjustGrids);
    // window.onclick = function(event){
    //     var large_video = document.getElementById("large_video");
    //     if(large_video.style.display == "block")
    //         large_video.style.display = "none";
    // }

    adjustGrids();

    //startMonitor();

    getCameraList();

    garageDoorOpener();

});

function closeLargeVideo(){
    alert("close large video");
}

function fullScreen() {

    var elem = document.documentElement;
    if (window.innerWidth == screen.width && window.innerHeight == screen.height) { // in full screen
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
    else {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        }
    }
}

function restartDaemons(daemon) {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        alert("The request to restart daemons was sent out!");
    }
    xhttp.open("GET", "/restart_daemons?name=" + daemon);
    xhttp.send();
}

function startMonitor() {

    const allcanvas = document.getElementsByClassName("video");
    for (let i = 0; i < allcanvas.length; i++) {

        try {

            let stream_port = $("#stream_port").val();

            loadPlayer({
                url: 'ws://' + window.location.hostname + ':' + stream_port + '/api/stream/' + allcanvas[i].id.substr(7, allcanvas[i].id.length - 7),
                canvas: allcanvas[i]
            });

        }
        catch (err) {

            console.log("loadPlayer error!");

        }

    }

}

function getCameraList() {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {

        const cameraList = JSON.parse(this.responseText);

        $("#stream_port").val(cameraList.port);

    }
    xhttp.open("GET", "/getCameraList?");
    xhttp.send();
}

window.addEventListener('DOMContentLoaded', function (e) {

    //// or, to resize all iframes:
    //var iframes = document.querySelectorAll("iframe");
    //for (var i = 0; i < iframes.length; i++) {
    //    resizeIFrameToFitContent(iframes[i]);
    //}
});

function closeLargeVideo() {
    $("#myModal").css({ display: "none" });
}

let garage_status_ws = null;

function garageDoorOpener() {
    let state = $("#garage_door_opener_state").html();
    let action;
    if (state == "unknown") {
        action = "state_garage_door";
        $("#garage_door_opener_state").html("get state...");
        $("#div_garage_door_opener").css("background-color", "green");
    }
    else if (state == "open") {
        action = "close_garage_door";
        $("#garage_door_opener_state").html("closing ...");
        $("#div_garage_door_opener").css("background-color", "green");
    }
    else if (state == "closed") {
        action = "open_garage_door";
        $("#garage_door_opener_state").html("opening ...");
        $("#div_garage_door_opener").css("background-color", "green");
    }
    else
        return;

    if (garage_status_ws == null) {
        let port = $("#daemon_status_ws_port").val();
        const service_status_Url = "ws://" + window.location.hostname + ":" + port;
        garage_status_ws = new WebSocket(service_status_Url);

        garage_status_ws.onopen = () => {
            garage_status_ws.send(action);
        }

        garage_status_ws.onerror = (error) => {
            garage_status_ws.close();
            logmsg("Service status WebSocket error");
        }

        garage_status_ws.onclose = (error) => {
            garage_status_ws = null;
        }

        garage_status_ws.onmessage = (e) => {
            $("#garage_door_opener_state").html(e.data);
            if (e.data == "open") {
                $("#garage_door_opener_action").html("close");
                $("#div_garage_door_opener").css("background-color", "rgb(100, 0, 0)");
            }
            else if (e.data == "closed") {
                $("#garage_door_opener_action").html("open");
                $("#div_garage_door_opener").css("background-color", "rgb(29, 51, 51)");
            }
            else {
                $("#garage_door_opener_action").html("unknown");
                $("#div_garage_door_opener").css("background-color", "rgb(29, 51, 51)");
            }

            //garage_status_ws.close();
        }
    }
    else
        garage_status_ws.send(action);

}

function bell() {

    var audio = new Audio(window.location.href + 'js?name=src/media/melodic-bell.wav');

    audio.play();
}

function beep(duration, type, finishedCallback) {
    var ctxClass = window.audioContext || window.AudioContext || window.AudioContext || window.webkitAudioContext
    var ctx = new ctxClass();

    duration = +duration;

    // Only 0-4 are valid types.
    type = (type % 5) || 0;

    if (typeof finishedCallback != "function") {
        finishedCallback = function () { };
    }

    var osc = ctx.createOscillator();

    //osc.type = type;
    osc.type = "sine";

    osc.connect(ctx.destination);
    if (osc.noteOn) osc.noteOn(0);
    if (osc.start) osc.start();

    setTimeout(function () {
        if (osc.noteOff) osc.noteOff(0);
        if (osc.stop) osc.stop();
        finishedCallback();
    }, duration);
};


let ws_push = null;

let ws_push_connect = function (url) {

    if ("WebSocket" in window) {

        ws_push = new WebSocket(url);

        ws_push.onopen = function () {
            // Web Socket is connected, send data using send()
            console.log("Push notification server connected!");
            ws_push.send("Message from browser");
        };

        ws_push.onmessage = function (msg) {
            var received_msg = msg.data;

            console.log((new Date()).toLocaleString() + " : " + received_msg);

            bell();
            //beep(500, 2, () => { });
        };

        ws_push.onclose = function () {

            // websocket is closed.
            console.log("Connection to push notification server is closed...");

            ws_push = null;
            setTimeout(() => ws_push_connect(url), 3000);
        };

    }
    else
        alert("WebSocket Not supported by your browser!");
}
