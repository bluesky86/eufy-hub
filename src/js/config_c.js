
function saveConfigChanges(){

    let eufy_server_ip = $("#eufy_server_ip").html();
    let eufy_server_port = $("#eufy_server_port").html();
    let control_port = $("#control_port").html();
    let notification_port = $("#notification_port").html();
    let get_video_hours = $("#get_video_hours").val();
    let download_timer_minutes = $("#download_timer_minutes").val();
    let heartbeat_timer_seconds = $("#heartbeat_timer_seconds").val();
    let schemaVersion = $("#schemaVersion").val();
    let download_path = $("#download_path").val();
    let keep_record_months = $("#keep_record_months").val();
    let default_device_sn = $("#default_device_sn").val();
    let log_obj_to_console = $("#log_obj_to_console").val();
    let log_file = $("#log_file").val();
    let log_to_console = $("#log_to_console").val();
    let restart_services = $("#restart_services").val();
    let event_dpi = $("#event_dpi").val();

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log("ok");
    }

    let value = { 
        ws_server: eufy_server_ip,
        ws_port: eufy_server_port,
        ws_ctrl_port: control_port,
        ws_push_notification_port: notification_port,
        get_video_hours: get_video_hours,
        download_timer_minutes: download_timer_minutes,
        heartbeat_timer_seconds: heartbeat_timer_seconds,
        schemaVersion: schemaVersion,
        default_device_sn: default_device_sn,
        download_path: download_path,
        keep_record_months: keep_record_months,
        log_obj_to_console: log_obj_to_console,
        log_to_console: log_to_console,
        log_file: log_file,
        restart_services: restart_services,
        event_dpi: event_dpi
    };
    xhttp.open("GET", "/saveConfig?name=" + JSON.stringify(value));
    xhttp.send();

    
}

function saveCameraChanges() {

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log("ok");
    }

    let value = { cameras: getCameras() };
    xhttp.open("GET", "/saveCameras?name=" + JSON.stringify(value));
    xhttp.send();

}

function addCamera() {

    let cameras = getCameras();

    cameras.push({
        name: "",
        device_sn: "",
        url: "",
        dpi: "",
        audio_sample: "",
        notify: false
    });

    let html = createCameraTable(cameras);

    $("#camera_table").html(html);

    $("#numOfCamera").val(cameras.length);


}

function deleteCamera(index) {

    let cameras = getCameras();
    let cameras_new = [];

    for (let i = 0; i < cameras.length; i++) {

        if (i != index)
            cameras_new.push(cameras[i])
    }

    let html = createCameraTable(cameras_new);

    $("#camera_table").html(html);

    $("#numOfCamera").val(cameras_new.length);

}

function moveUp(index) {

    if (index == 0) return;

    let cameras = getCameras();
    let cameras_new = [];

    for (let i = 0; i < cameras.length; i++) {

        if (i == index - 1) {
            cameras_new.push(cameras[i + 1])
            cameras_new.push(cameras[i])
            i++;
        }
        else
            cameras_new.push(cameras[i])
    }

    let html = createCameraTable(cameras_new);

    $("#camera_table").html(html);

}

function moveDown(index) {

    let cameras = getCameras();
    if (index == cameras.length - 1) return;

    let cameras_new = [];

    for (let i = 0; i < cameras.length; i++) {

        if (i == index) {
            cameras_new.push(cameras[i + 1])
            cameras_new.push(cameras[i])
            i++;
        }
        else
            cameras_new.push(cameras[i])
    }

    let html = createCameraTable(cameras_new);

    $("#camera_table").html(html);

}

function createCameraTable(cameras) {

    let res = "<colgroup><col span='7' style='background-color:bisque'></colgroup>";
    res += "<tr>";
    res += "<th style='text-align: center; width: 10%'>camera name</th>";
    res += "<th style='text-align: center; width: 20%'>device sn</th>";
    res += "<th style='text-align: center; width: 40%'>rtsp url</th>";
    res += "<th style='text-align: center; width: 40%'>rtsp dpi</th>";
    res += "<th style='text-align: center; width: 12%'>audio sample</th>";
    res += "<th style='text-align: center; width: 10%'>notification</th>";
    // res += "<th style='text-align: center; width: 15px'>cell class</th>";
    res += "<th style='text-align: center; width: 15px'>action</th>";
    res += "</tr>";

    for (let i = 0; i < cameras.length; i++) {

        res += `
            <tr>
            <td>
                <input type='text' id='row${i}_name' value='${cameras[i].name}' style='width:100px;'>              
            </td>
            <td>
                <input type='text' id='row${i}_device_sn' value='${cameras[i].device_sn}' style='width:150px;'>              
            </td>
            <td>
                <input type='text' id='row${i}_url' value='${cameras[i].url}' style='width:400px;'>
            </td>
            <td>
                <input type='text' id='row${i}_dpi' value='${cameras[i].dpi}' style='width:80px;'>
            </td>
            <td>
                <input type='text' id='row${i}_audio_sample' value='${cameras[i].audio_sample}' style='width:60px;'>
            </td>
            <td>
                <input type='checkbox' id='row${i}_notify' style='width:20px;' ${(cameras[i].notify == true)?"checked":""}>
            </td>
            <td>
                <table><tr>
                    <td><button type="button" onclick='moveUp(${i})'>Up</button></td>
                    <td><button type="button" onclick='moveDown(${i})'>Down</button></td>
                    <td><button type="button" onclick='deleteCamera(${i})'>Delete</button></td>
                </tr></table>
            </td>
            </tr>
        `;
    }

    return res;
}

function getCameras() {

    let numRows = $("#numOfCamera").val();
    let cameras = [];

    for (let i = 0; i < numRows; i++) {

        let name = $("#row" + i.toString() + "_name").val();
        let device_sn = $("#row" + i.toString() + "_device_sn").val();
        let url = $("#row" + i.toString() + "_url").val();
        let dpi = $("#row" + i.toString() + "_dpi").val();
        let audio_sample = $("#row" + i.toString() + "_audio_sample").val();
        let notify = $("#row" + i.toString() + "_notify")[0].checked;

        cameras.push({
            name: name,
            device_sn: device_sn,
            url: url,
            dpi: dpi,
            audio_sample: audio_sample,
            notify: notify
        });
    }

    return cameras;

}

function saveCellChanges() {

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log("ok");
    }

    let value = { cells: getCells() };
    let str_value = JSON.stringify(value);
    console.log(str_value);
    xhttp.open("GET", "/saveCells?name=" + str_value);
    xhttp.send();

}

function addCell() {

    let cells = getCells();

    cells.push({
        name: "",
        type: "",
        width: 1,
        height: 1,
        url: "",
        icon: "",
        x: 1,
        y: 1,
        border_left_width: "1px",
        border_right_width: "1px",
        border_top_width: "1px",
        border_bottom_width: "1px",
        enable: true
    });

    let html = createCellTable(cells);

    $("#cell_table").html(html);

    $("#numOfCell").val(cells.length);


}

function deleteCell(index) {

    let cells = getCells();
    let cells_new = [];

    for (let i = 0; i < cells.length; i++) {

        if (i != index)
            cells_new.push(cells[i])
    }

    let html = createCellTable(cells_new);

    $("#cell_table").html(html);

    $("#numOfCell").val(cells_new.length);

}

function moveCellUp(index) {

    if (index == 0) return;

    let cells = getCells();
    let cells_new = [];

    for (let i = 0; i < cells.length; i++) {

        if (i == index - 1) {
            cells_new.push(cells[i + 1])
            cells_new.push(cells[i])
            i++;
        }
        else
            cells_new.push(cells[i])
    }

    let html = createCellTable(cells_new);

    $("#cell_table").html(html);

}

function moveCellDown(index) {

    let cells = getCells();
    if (index == cells.length - 1) return;

    let cells_new = [];

    for (let i = 0; i < cells.length; i++) {

        if (i == index) {
            cells_new.push(cells[i + 1])
            cells_new.push(cells[i])
            i++;
        }
        else
            cells_new.push(cells[i])
    }

    let html = createCellTable(cells_new);

    $("#cell_table").html(html);

}

function createCellTable(cells) {

    let res = "<colgroup><col span='14' style='background-color:bisque'></colgroup>";
    res += "<tr>";
    res += "<th style='text-align: center;'>name</th>";
    res += "<th style='text-align: center;'>type</th>";
    res += "<th style='text-align: center;'>width</th>";
    res += "<th style='text-align: center;'>height</th>";
    res += "<th style='text-align: center;'>y</th>";
    res += "<th style='text-align: center;'>x</th>";
    res += "<th style='text-align: center;'>url</th>";
    res += "<th style='text-align: center;'>icon</th>";
    res += "<th style='text-align: center;'>border left</th>";
    res += "<th style='text-align: center;'>border right</th>";
    res += "<th style='text-align: center;'>border top</th>";
    res += "<th style='text-align: center;'>border bottom</th>";
    res += "<th style='text-align: center;'>Enable</th>";
    res += "<th style='text-align: center;'>action</th>";
    res += "</tr>";

    for (let i = 0; i < cells.length; i++) {

        res += `
            <tr>
            <td>
                <input type='text' id='cell_row${i}_name' value='${cells[i].name}' style='width:100px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_type' value='${cells[i].type}' style='width:60px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_width' value='${!cells[i].width ? 1 : cells[i].width}' style='width:15px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_height' value='${!cells[i].height ? 1 : cells[i].height}' style='width:15px;'>              
            </td>
            <td>
                <input type='text' id='cell_row${i}_y' value='${!cells[i].y ? 0 : cells[i].y}' style='width:15px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_x' value='${!cells[i].x ? 0 : cells[i].x}' style='width:15px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_url' value="${!cells[i].url ? "" : cells[i].url}" style='width:200px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_icon' value='${!cells[i].icon ? "" : cells[i].icon}' style='width:100px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_left' value='${cells[i].border_left_width}' style='width:30px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_right' value='${cells[i].border_right_width}' style='width:30px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_top' value='${cells[i].border_top_width}' style='width:30px;'>
            </td>
            <td>
                <input type='text' id='cell_row${i}_border_bottom' value='${cells[i].border_bottom_width}' style='width:30px;'>
            </td>
            <td>
                <input type='checkbox' id='cell_row${i}_enable' ${cells[i].enable ? "checked":""} style='width:30px;'>
            </td>
            <td>
                <table><tr>
                    <td><button type="button" onclick='moveCellUp(${i})'>Up</button></td>
                    <td><button type="button" onclick='moveCellDown(${i})'>Down</button></td>
                    <td><button type="button" onclick='deleteCell(${i})'>Delete</button></td>
                </tr></table>
            </td>
            </tr>
        `;
    }

    return res;
}

function getCells() {

    let numRows = $("#numOfCell").val();
    let cells = [];

    for (let i = 0; i < numRows; i++) {

        let name = $("#cell_row" + i.toString() + "_name").val();
        let type = $("#cell_row" + i.toString() + "_type").val();
        let width = $("#cell_row" + i.toString() + "_width").val();
        let height = $("#cell_row" + i.toString() + "_height").val();
        let x = $("#cell_row" + i.toString() + "_x").val();
        let y = $("#cell_row" + i.toString() + "_y").val();
        let url = $("#cell_row" + i.toString() + "_url").val();
        let icon = $("#cell_row" + i.toString() + "_icon").val();
        let border_left = $("#cell_row" + i.toString() + "_border_left").val();
        let border_right = $("#cell_row" + i.toString() + "_border_right").val();
        let border_top = $("#cell_row" + i.toString() + "_border_top").val();
        let border_bottom = $("#cell_row" + i.toString() + "_border_bottom").val();
        let enable_v = $("#cell_row" + i.toString() + "_enable")[0].checked;

        cells.push({
            name: name,
            type: type,
            width: parseInt(width),
            height: parseInt(height),
            x: parseInt(x),
            y: parseInt(y),
            url: url,
            icon: icon,
            border_left_width: border_left,
            border_right_width: border_right,
            border_top_width: border_top,
            border_bottom_width: border_bottom,
            enable: enable_v
        });
    }

    return cells;

}

function fullScreen() {

    var elem = document.documentElement;
    if (window.innerWidth == screen.width && window.innerHeight == screen.height) { // in full screen
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
    else {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        }
    }
}

function restartDaemons(daemon) {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        alert("The request to restart daemons was sent out!");
    }
    xhttp.open("GET", "/restart_daemons?name=" + daemon);
    xhttp.send();
}
