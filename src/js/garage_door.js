/* jshint -W097 */
/* jshint -W030 */
/* jshint strict:true */
/* jslint node: true */
/* jslint esversion: 6 */
'use strict';

const MerossCloud = require('../js/meross.js');

const options = {
    'email': 'txyyxl@gmail.com',
    'password': 'Lucent-01',
    //'logger': console.log
};


const meross = new MerossCloud(options);
let device_opener = {
    device: null,
    status_wsClients: []
};

meross.on('deviceInitialized', (deviceId, deviceDef, device) => {
    // console.log('New device ' + deviceId + ': ');
    // console.log(deviceDef);

    device_opener.device = device;

    device.on('connected', () => {
        // console.log('DEV: ' + deviceId + ' connected');

        device.getSystemAbilities((err, res) => {
            // console.log('Abilities: ');
            // console.log(res);

            // device.getSystemAllData((err, res) => {
            //     console.log('All-Data: ');
            //     console.log(res);
            //     if(res.all.digest.garageDoor.length > 0 && res.all.digest.garageDoor[0].open == 1)
            //         console.log((new Date(res.all.digest.garageDoor[0].lmTime) * 1000).toLocaleString() + " door is open!")
            //     else
            //         console.log((new Date(res.all.digest.garageDoor[0].lmTime * 1000)).toLocaleString() + " door is closed!");
            // });
        });

        // setTimeout(() => {

        //     device.controlGarageDoor(0, false, (data, msg) => {

        //         console.log("control garage door");
        //         console.log(data);
        //         console.log(msg);
        //     });
        // }, 5000);
    });

    device.on('close', (error) => {
        device_opener.device_opener = null;
        device_opener.status_wsClients = [];
        //console.log('CLOSE: ' + deviceId + ' closed: ' + error);
    });

    device.on('error', (error) => {
        //console.log('ERROR: ' + deviceId + ' error: ' + error);
    });

    device.on('reconnect', () => {
        //console.log('RECONNECT: ' + deviceId + ' reconnected');
    });

    device.on('data', (namespace, payload) => {
        //console.log('DATA: ' + deviceId + ' ' + namespace + ' - data: ');
        //console.log(payload);
        for (let i = 0; i < device_opener.status_wsClients.length; i++) {

            if (device_opener.status_wsClients[i].readyState == 1) {

                if (payload.state[0].open == 1) {
                    device_opener.status_wsClients[i].send("open");
                    //console.log("closed -> open");
                }
                else {
                    device_opener.status_wsClients[i].send("closed");
                    //console.log("open -> closed");
                }
            }
        }

    });

});

meross.on('connected', (deviceId) => {
    // console.log(deviceId + ' connected');
});

meross.on('close', (deviceId, error) => {
    // console.log(deviceId + ' closed: ' + error);
});

meross.on('error', (deviceId, error) => {
    // console.log(deviceId + ' error: ' + error);
});

meross.on('reconnect', (deviceId) => {
    // console.log(deviceId + ' reconnected');
});

meross.on('data', (deviceId, payload) => {
    // console.log(deviceId + ' data: ');
    // console.log(payload);
});



meross.connect((error) => {
    if (error)
        console.log('connect error: ' + error);
});

exports.meross = meross;
exports.device_opener = device_opener;
