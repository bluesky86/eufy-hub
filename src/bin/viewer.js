
const http = require("http");
const fs = require("fs");
const url = require('url');
const utils = require("../js/utils.js");
const status_ws = require("../js/status_ws.js");

const get_video = require("../js/getvideos.js");

const web_port = 3030;

const server = http.createServer(function (req, res) {

    //res.write("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js'></script>\n"); 
    //res.write("<script https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js'></script>\n"); 
    //res.write("<script src='./src/bin/clientHub.js'></script>\n"); 

    if (req.url == "/") {
        try {

            //const config = utils.loadJsonFile("config.json");
            const rtsp_relay = require("../js/start_relay.js");
            const cell = require("../js/cell.js");

            res.write(fs.readFileSync("./src/html/index_head.html"));
            res.write(cell.createCells());
            res.write(`
                <script>
                    ws_push_connect("ws://" + window.location.hostname + ":${get_video.config.ws_push_notification_port}");
                </script>
                <input type="hidden" id="daemon_status_ws_port" value="${utils.cameras.ws_daemon_status_port}">
            `);
            res.write(fs.readFileSync("./src/html/index_nav.html"));

        } catch (err) {
            res.write("Error:" + err);
        }
        res.end();
    }
    else if(req.url == "/relay"){
        res.write(fs.readFileSync("./src/html/relay.html"));
        res.end();
    }
    else if (req.url == "/events") {
        try {
            res.write("<!DOCTYPE html>\n");
            res.write("<html>\n");
            res.write("<link rel='stylesheet' href='/css?name=events.css'>");
            res.write(fs.readFileSync("./src/html/events_head.html"));
            res.write(fs.readFileSync("./src/html/events_body.html"));
            res.write("\n</html>\n");
        } catch (err) {
            res.write("File doesn't exist");
        }
        res.end();
    }
    else if(req.url == "/config"){
        const configjs = require("../js/config_s.js");
        res.write("<!DOCTYPE html>\n");
        res.write("<html>\n");
        res.write("<link rel='stylesheet' href='/css?name=events.css'>");
        res.write(fs.readFileSync("./src/html/config_start.html"));
        res.write(configjs.createConfigPage());
        res.write(configjs.createCameraPage());
        res.write(fs.readFileSync("./src/html/config_end.html"));
        res.write("\n</html>\n");
        res.end();
    }
    else if(req.url.indexOf("/saveConfig?") == 0){ // for config

        let parms = url.parse(req.url, true).query;

        let new_config = JSON.parse(parms.name);

        fs.writeFileSync("config.json", JSON.stringify(new_config));

        res.write("ok");
        res.end();
    }
    else if(req.url.indexOf("/saveCameras?") == 0){ // for config

        let parms = url.parse(req.url, true).query;

        let cameras = JSON.parse(parms.name);

        let old_cameras = utils.cameras;
        old_cameras.cameras = cameras.cameras;

        fs.writeFileSync("camera.json", JSON.stringify(old_cameras));

        res.write("ok");
        res.end();
    }
    else if(req.url.indexOf("/getCameraList?") == 0){ // for events

        let cameraList = JSON.stringify(utils.cameras);
        res.write(cameraList);

        res.end();
    }
    else if(req.url.indexOf("/saveCells?") == 0){ // for config

        const cellsjs = require("../js/cell.js");

        let decodedStr = utils.escapeAnd(req.url);

        let parms = url.parse(decodedStr, true).query;

        let cells = JSON.parse(parms.name);

        let old_cells = cellsjs.cells;
        old_cells.cells = cells.cells;

        fs.writeFileSync("cell.json", JSON.stringify(old_cells));

        res.write("ok");
        res.end();
    }
    else if (req.url.indexOf("/getVideoList?") == 0) {

        let parms = url.parse(req.url, true).query;

        const videoList = get_video.getVideoList(utils.getDeviceSN(parms.device), parms.date);

        const value = JSON.stringify(videoList);

        res.write(value);
        res.end();
    }
    else if (req.url.indexOf("/play?") == 0) {

        let parms = url.parse(req.url, true).query;

        const value = get_video.getVideoUrl(parms.device, parms.date, parms.file);

        if (parms.player == "ffmpeg") {
            
            const rtsp_relay = require("../js/start_relay.js");

            rtsp_relay.forceKill();

            if(!parms.kill) {
                rtsp_relay.createHandle({
                    name: parms.file,
                    //url: "http://" + utils.getIpAddress() + ":" + web_port.toString() + value.url,
                    url: value.url,//"http://" + get_video.config.ws_server + ":" + web_port.toString() + value.url,
                    hasAudioFile: value.hasAudioFile,
                    dpi: get_video.config.event_dpi
                });
            }
        }

        res.write(JSON.stringify(value));

        res.end();
    }
    else if (req.url.indexOf("/stream?") == 0) {

        let parms = url.parse(req.url, true).query;
        let data = get_video.getStream(parms.device, parms.date, parms.file);
        res.write(data);

        res.end();
    }
    else if (req.url.indexOf("/js?") == 0) {

        let parms = url.parse(req.url, true).query;

        let data = fs.readFileSync(parms.name);

        res.write(data);

        res.end();
    }
    else if (req.url.indexOf("/css?") == 0) {

        let parms = url.parse(req.url, true).query;

        let data = fs.readFileSync("src/css/" + parms.name);

        res.write(data);

        res.end();
    }
    else if (req.url.indexOf("/restart_daemons") == 0) {

        let parms = url.parse(req.url, true).query;

        get_video.restartDaemons(parms.name);

        res.write("OK");

        res.end();
    }
    else if(req.url.indexOf("/test?") == 0) {
        res.write(fs.readFileSync("./src/html/test1.html"));
        res.end();
    }
    else if(req.url.indexOf("/teststream?") == 0) {
        let parms = url.parse(req.url, true).query;
        if (fs.existsSync(parms.name)) {
            //let data = fs.readFileSync(parms.name);
            //res.write(data);
            fs.readFile(parms.name, (err, data) => {
                res.write(data);
                res.end();
            });
        }
    }
    else if(req.url.indexOf("/videoconvert?") == 0) {
        const ffmpeg = require("../js/convert.js").ffmpeg_convert_func();
        let parms = url.parse(req.url, true).query;
        const path = get_video.getVideoUrl(parms.device, parms.date, parms.file);

        if (fs.existsSync(path.url + '.' + parms.type)) {
            res.write('ok');
            res.end();
        }
        else {
            let handler = ffmpeg.create({
                url: path.url,
                targetFile: path.url,
                videoType: parms.type,
                verbose: false,
                hasAudioFile: path.hasAudioFile,
                dpi: get_video.config.event_dpi,
                errorCallback: (err) => {
                    res.write(err);
                    res.end();
                },
                endCallback: () => {
                    res.write('ok');
                    res.end();
                }
            });
            handler.start();
        }
    }
    else if(req.url.indexOf("/html5?") == 0) {
        let parms = url.parse(req.url, true).query;
        const path = get_video.getVideoUrl(parms.device, parms.date, parms.file);
        
        if (path.result !== 'failure') {
            let data = fs.readFileSync(path.url);
            res.write(data);
         }
         else {
            console.log("File " + parms.file + " does not exist!")
         }

        res.end();
   }
    // else if (req.url == "getServerInfo") {
    //     let ip = utils.getIpAddress();
    // }

});

server.listen(web_port); //6 - listen for any incoming requests

console.log("Node.js web server at port " + web_port.toString() + " is running..");


