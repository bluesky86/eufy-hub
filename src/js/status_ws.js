
const utils = require("../js/utils.js");
const fs = require("fs");
//const config = utils.loadJsonFile("config.json");
const { exec } = require('child_process');
const WebSocket = require("ws");
const garage_door = require("../js/garage_door.js");

let wss = new WebSocket.Server({ port: utils.cameras.ws_daemon_status_port })

let statusWsClients = [];

wss.on("connection", ws => {

    // remove disconnected clients
    let j = 0;
    for (let i = 0; i < statusWsClients.length; i++) {
        if (statusWsClients[i].readyState == 1) {
            if (i > j) {
                statusWsClients[j] = statusWsClients[i];
                j++;
            }
        }
    }
    statusWsClients.push(ws);

    garage_door.device_opener.status_wsClients = statusWsClients;

    ws.on("message", message => {
        let msg = message.toString();
        if (msg == "server") {
            exec(" systemctl status eufy-security-ws.service", (err, stdout, stderr) => {
                if (stdout) {
                    ws.send(stdout.toString());
                    stdout.write('\x1Bc');
                }
                if (stderr) {
                    ws.send(stderr.toString());
                }
                if (err) {
                    ws.send(err.toString());
                } else {
                    ws.send("unknown!");
                }
            });
        }
        else if (msg == "hub") {
            exec(" systemctl status eufy-hub.service", (err, stdout, stderr) => {
                if (stdout) {
                    ws.send(stdout.toString());
                    stdout.write('\x1Bc');
                }
                if (stderr) {
                    ws.send(stderr.toString());
                }
                if (err) {
                    ws.send(err.toString());
                } else {
                    ws.send("unknown!");
                }
            });
        }
        else if (msg == "viewer") {
            exec("systemctl status eufy-viewer.service", (err, stdout, stderr) => {
                if (stdout) {
                    ws.send(stdout.toString());
                    stdout.write('\x1Bc');
                }
                if (stderr) {
                    ws.send(stderr.toString());
                }
                if (err) {
                    ws.send(err.toString());
                } else {
                    ws.send("unknown!");
                }
            });
        }
        else if (msg == "monitor") {
            exec("systemctl status eufy-monitor.service", (err, stdout, stderr) => {
                if (stdout) {
                    ws.send(stdout.toString());
                    stdout.write('\x1Bc');
                }
                if (stderr) {
                    ws.send(stderr.toString());
                }
                if (err) {
                    ws.send(err.toString());
                } else {
                    ws.send("unknown!");
                }
            });
        }
        else if (msg == "show_log") {
            let config = utils.loadJsonFile("config.json");
            if (!fs.existsSync(config.log_file)){
                fs.writeFileSync(config.log_file, "", { encoding: "utf8", flag: "w" });
            }
            let log = fs.readFileSync(config.log_file);
            ws.send(log.toString());
        }
        else if (msg == "clear_log") {
            let config = utils.loadJsonFile("config.json");
            fs.writeFileSync(config.log_file, "", { encoding: "utf8", flag: "w" });
            ws.send("done");
        }
        else if (msg == "disk") {

            const disk = require('diskusage');
            disk.check('/', function (err, info) {

                const data = {
                    free: info.free,
                    total: info.total
                };

                ws.send(JSON.stringify(data));

            });

        }
        else if (msg == "state_garage_door") {

            let interval = 0;
            if (!garage_door.device_opener.device)
                interval = 3000;

            setTimeout(() =>
                garage_door.device_opener.device && garage_door.device_opener.device.getSystemAllData((err, res) => {
                    //console.log(res);
                    if (res && res.all && res.all.digest.garageDoor.length > 0 && res.all.digest.garageDoor[0].open == 1) {
                        ws.send("open");
                        //console.log((new Date(res.all.digest.garageDoor[0].lmTime) * 1000).toLocaleString() + " door is open!")
                    }
                    else {
                        ws.send("closed");
                        //console.log((new Date(res.all.digest.garageDoor[0].lmTime * 1000)).toLocaleString() + " door is closed!");
                    }
                }), interval);

        }
        else if (msg == "open_garage_door") {

            garage_door.device_opener.device.controlGarageDoor(0, true, (data, msg) => {
                /////// commented below codes out since garage_door.js onmessage will send notification
                // if (msg.state.open + msg.state.execute == 1)
                //     ws.send("open");
                // else
                //     ws.send("closed");
            });

        }
        else if (msg == "close_garage_door") {

            garage_door.device_opener.device.controlGarageDoor(0, false, (data, msg) => {
                /////// commented below codes out since garage_door.js onmessage will send notification
                // if (msg.state.open + msg.state.execute == 1)
                //     ws.send("open");
                // else
                //     ws.send("closed");
            });

        }
    });


});

module.exports = statusWsClients;

console.log("Service status web socket at port " + utils.cameras.ws_daemon_status_port.toString() + " is running..");
